import Head from "next/head";
import React from "react";

type Props = {};

const Seo = (props: Props) => {
  return (
    <Head>
      <title>Baby Monster In Jakarta</title>
      <meta charSet="UTF-8" />
      <meta name="description" content="Baby Monster In Jakarta" />
      <meta name="keyword" content="Baby Monster" />
    </Head>
  );
};

export default Seo;
