import React from "react";

type Props = {};

const Comingsoon = (props: Props) => {
  return (
    <div className="flex justify-center items-center text-[30px] font-bold h-[100vh]">
      Coming Soon
    </div>
  );
};

export default Comingsoon;
