import Image from "next/image";
import { Inter } from "next/font/google";
import headLogo from "@/public/images/head.png";
import hero from "@/public/images/hero.jpg";
import layout from "@/public/images/layout.jpg";
import temLogo from "@/public/images/temlogo.png";
import pkLogo from "@/public/images/pklogo.png";
import vid1 from "@/public/images/vid1.webp";
import vid2 from "@/public/images/vid2.webp";
import Seo from "@/components/Seo";
import Link from "next/link";
import { useEffect, useState } from "react";
import Comingsoon from "@/components/Comingsoon";
import { useRouter } from "next/router";

import procedure1 from "@/public/images/BM-Social Media-Wristband Redemption Procedure-01.jpg";
import procedure2 from "@/public/images/BM-Social Media-Wristband Redemption Procedure-02.jpg";
import procedure3 from "@/public/images/BM-Social Media-Wristband Redemption Procedure-03.jpg";
import procedure4 from "@/public/images/BM-Social Media-Wristband Redemption Procedure-04.jpg";
import procedure5 from "@/public/images/BM-Social Media-Wristband Redemption Procedure-05.jpg";
import Lottie from "lottie-react";
import procedure6 from "@/public/images/6.jpg";
import swipe from "@/public/lottie/swipe.json";

import winner1 from "@/public/images/winner/BMS24 - Social Media-Poster Winner-01.jpeg";
import winner2 from "@/public/images/winner/BMS24 - Social Media-Poster Winner-02.jpeg";
import winner3 from "@/public/images/winner/BMS24 - Social Media-Poster Winner-03.jpeg";
import winner4 from "@/public/images/winner/BMS24 - Social Media-Poster Winner-04.jpeg";
import winner5 from "@/public/images/winner/BMS24 - Social Media-Poster Winner-05.jpeg";
import winner6 from "@/public/images/winner/BMS24 - Social Media-Poster Winner-06.jpeg";
import winner7 from "@/public/images/winner/BMS24 - Social Media-Poster Winner-07.jpeg";
import winner8 from "@/public/images/winner/BMS24 - Social Media-Poster Winner-08.jpeg";

import groupWinner1 from "@/public/images/winner2/BMS24 - Social Media-Group Photo Winner-01.jpeg";
import groupWinner2 from "@/public/images/winner2/BMS24 - Social Media-Group Photo Winner-02.jpeg";
import groupWinner3 from "@/public/images/winner2/BMS24 - Social Media-Group Photo Winner-03.jpeg";
import groupWinner4 from "@/public/images/winner2/BMS24 - Social Media-Group Photo Winner-04.jpeg";

import {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Autoplay,
} from "swiper/modules";

import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

export default function Home() {
  const [showAll, setShowAll] = useState(false);

  const toggleShowAll = () => {
    setShowAll(!showAll);
  };

  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  const [isVisible, setIsVisible] = useState(false);

  const [showButton, setShowButton] = useState(false);

  useEffect(() => {
    const reloadIfTimeMatches = () => {
      const targetDate = new Date("2024-04-22T14:00:00+07:00");
      const currentDate = new Date();

      if (currentDate.getTime() === targetDate.getTime()) {
        window.location.reload();
      } else {
        const timeUntilReload = targetDate.getTime() - currentDate.getTime();
        setTimeout(reloadIfTimeMatches, timeUntilReload);

        setShowButton(currentDate >= targetDate);
      }
    };

    reloadIfTimeMatches();
  }, []);

  const router = useRouter();

  useEffect(() => {
    const handleScroll = () => {
      const aboutSection = document.getElementById("about");
      if (aboutSection) {
        const aboutSectionPosition = aboutSection.getBoundingClientRect();
        setIsVisible(aboutSectionPosition.top < window.innerHeight);
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  const [isButtonDisabled, setIsButtonDisabled] = useState(false);
  useEffect(() => {
    const checkDisableButton = () => {
      const targetTime = new Date("2024-05-02T10:00:00+07:00");

      const currentTime = new Date();

      setIsButtonDisabled(currentTime < targetTime);
    };

    checkDisableButton();
    const interval = setInterval(checkDisableButton, 1000); // Cek setiap menit

    return () => clearInterval(interval);
  }, []);

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  return (
    <>
      <Seo />
      <header className="w-full bg-white flex">
        <nav className="max-w-[1366px] mx-auto lg:px-10 px-5 flex gap-10 justify-between items-center py-2">
          <div className="lg:w-4/12 w-10/12">
            <Image className="w-full" src={headLogo} alt="Logo Promotor" />
          </div>
          <div className="w-2/12 lg:w-4/12">
            {/* Hamburger menu icon */}
            <div className="block lg:hidden" onClick={toggleMenu}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6 cursor-pointer"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 6h16M4 12h16m-7 6h7"
                />
              </svg>
            </div>
          </div>

          {isMenuOpen && (
            <div className="absolute right-0 top-10 mt-2 bg-white shadow-lg rounded-lg py-2 px-2 w-48">
              <div className="mb-2">
                <Link href="#about">
                  <div className="hover:text-red-500 cursor-pointer">NEWS</div>
                </Link>
              </div>
              <div className="mb-2">
                <Link href="#layout">
                  <div className="hover:text-red-500 cursor-pointer">
                    TICKETS
                  </div>
                </Link>
              </div>
              <div className="mb-2">
                <Link href="#promoters">
                  <div className="hover:text-red-500 cursor-pointer">
                    PROMOTERS
                  </div>
                </Link>
              </div>
              <div>
                <Link href="#contact">
                  <div className="hover:text-red-500 cursor-pointer">
                    CONTACT US
                  </div>
                </Link>
              </div>
            </div>
          )}

          <div
            className={`w-full lg:w-4/12 font-bold lg:flex justify-between items-center hidden`}
          >
            <Link href="#about">
              <div className="hover:text-red-500 cursor-pointer transition-all duration-200 ease-out">
                NEWS
              </div>
            </Link>
            <Link href="#layout">
              <div className="hover:text-red-500 cursor-pointer transition-all duration-200 ease-out">
                TICKETS
              </div>
            </Link>
            <Link href="#promoters">
              <div className="hover:text-red-500 cursor-pointer transition-all duration-200 ease-out">
                PROMOTERS
              </div>
            </Link>
            <Link href="#contact">
              <div className="hover:text-red-500 cursor-pointer transition-all duration-200 ease-out">
                CONTACT US
              </div>
            </Link>
          </div>
        </nav>
      </header>

      <main className="w-full">
        <Image className="w-full" src={hero} alt="Poster" />
      </main>

      <div className="max-w-[1366px] mx-auto lg:px-10 px-7 py-10">
        <div
          id="about"
          className="flex flex-col gap-5 items-center border-b-2 pb-10 border-gray-300"
        >
          <div className="text-[30px] font-bold font-Montserrat">News</div>
          <div className="flex flex-col items-center gap-3">
            <p>
              TEM Presents and PK Entertainment proudly present the newest K-Pop
              girl group from YG Entertainment, BABYMONSTER, who will be holding
              their First Fanmeet 2024 ‘See You There’ in Jakarta on June 8,
              2024 at Tennis Indoor Senayan in Jakarta. This will be
              BABYMONSTER’s first appearance in Indonesia since their debut on
              November 26, 2023.
            </p>
            <p>
              The group, with multinational members from Korea, Thailand, and
              Japan, imprinted the presence in the global music market as the
              "monster rookie" with overwhelming capabilities covering all
              aspects from vocal, rap, performance to visuals.
            </p>
            <p>
              Following two pre debut singles: "BATTER UP" and "Stuck In The
              Middle," BABYMONSTER made its official debut with the first EP,
              {" [BABYMONS7ER]"}, on April 1st, 2024. Title track "SHEESH"
              ranked 69th on Spotify's daily top song global chart on the first
              day of its release.
            </p>
          </div>
        </div>

        {/* Redemption */}

        <div
          id="carousel"
          className="flex flex-col gap-5 items-center border-b-2 pb-10 pt-7 border-gray-300"
        >
          <div className="lg:text-[30px] text-[24px] lg:leading-normal leading-5 font-bold font-Montserrat text-center">
            Wristband Redemption Procedure
          </div>
          {/* Desktop */}
          <div className="w-full lg:flex flex-col items-center hidden ">
            <Swiper
              modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
              spaceBetween={50}
              slidesPerView={2}
              speed={1500}
              scrollbar={{ draggable: true }}
              autoplay={{ delay: 5000 }}
              className="h-[650px] w-full"
            >
              <SwiperSlide>
                <div>
                  <Image src={procedure1} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={procedure2} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={procedure3} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={procedure4} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={procedure5} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={procedure6} alt="carousel" />
                </div>
              </SwiperSlide>
            </Swiper>
            <div className="w-1/12  -mt-24 z-50">
              <Lottie animationData={swipe} loop={true} />
            </div>
          </div>
          {/* Mobile */}
          <div className="w-full lg:hidden flex flex-col items-center">
            <Swiper
              modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
              spaceBetween={50}
              slidesPerView={1}
              speed={1000}
              scrollbar={{ draggable: true }}
              autoplay={{ delay: 2000 }}
              className="h-full  w-full"
            >
              <SwiperSlide>
                <div>
                  <Image src={procedure1} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={procedure2} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={procedure3} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={procedure4} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={procedure5} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={procedure6} alt="carousel" />
                </div>
              </SwiperSlide>
            </Swiper>
            <div className="w-4/12  -mt-1 z-50 text-center">
              <Lottie animationData={swipe} loop={true} />
            </div>
          </div>
        </div>

        {/* Winners  1*/}

        <div
          id="carousel"
          className="flex flex-col gap-5 items-center border-b-2 pb-10 pt-7 border-gray-300"
        >
          <div className="lg:text-[30px] text-[24px] lg:leading-normal leading-5 font-bold font-Montserrat text-center">
            Signed Poster Raffle Winners
          </div>
          {/* Desktop */}
          <div className="w-full lg:flex flex-col items-center hidden ">
            <Swiper
              modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
              spaceBetween={50}
              slidesPerView={2}
              scrollbar={{ draggable: true }}
              autoplay={{ delay: 5000 }}
              className="h-[650px] w-full"
            >
              <SwiperSlide>
                <div>
                  <Image src={winner1} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner2} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner3} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner4} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner5} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner6} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner7} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner8} alt="carousel" />
                </div>
              </SwiperSlide>
            </Swiper>
            <div className="w-1/12  -mt-24 z-50">
              <Lottie animationData={swipe} loop={true} />
            </div>
          </div>
          {/* Mobile */}
          <div className="w-full lg:hidden flex flex-col items-center">
            <Swiper
              modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
              spaceBetween={50}
              slidesPerView={1}
              speed={1000}
              scrollbar={{ draggable: true }}
              autoplay={{ delay: 5000 }}
              className="h-full  w-full"
            >
              <SwiperSlide>
                <div>
                  <Image src={winner1} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner2} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner3} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner4} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner5} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner6} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner7} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={winner8} alt="carousel" />
                </div>
              </SwiperSlide>
            </Swiper>
            <div className="w-4/12  -mt-1 z-50 text-center">
              <Lottie animationData={swipe} loop={true} />
            </div>
          </div>
        </div>
        {/* Winner 2 */}

        <div
          id="carousel"
          className="flex flex-col gap-5 items-center border-b-2 pb-10 pt-7 border-gray-300"
        >
          <div className="lg:text-[30px] text-[24px] lg:leading-normal leading-5 font-bold font-Montserrat text-center">
            Group Photo Raffle Winners
          </div>
          {/* Desktop */}
          <div className="w-full lg:flex flex-col items-center hidden ">
            <Swiper
              modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
              spaceBetween={50}
              slidesPerView={2}
              scrollbar={{ draggable: true }}
              autoplay={{ delay: 5000 }}
              className="h-[650px] w-full"
            >
              <SwiperSlide>
                <div>
                  <Image src={groupWinner1} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={groupWinner2} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={groupWinner3} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={groupWinner4} alt="carousel" />
                </div>
              </SwiperSlide>
            </Swiper>
            <div className="w-1/12  -mt-24 z-50">
              <Lottie animationData={swipe} loop={true} />
            </div>
          </div>
          {/* Mobile */}
          <div className="w-full lg:hidden flex flex-col items-center">
            <Swiper
              modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
              spaceBetween={50}
              slidesPerView={1}
              speed={1000}
              scrollbar={{ draggable: true }}
              autoplay={{ delay: 5000 }}
              className="h-full  w-full"
            >
              <SwiperSlide>
                <div>
                  <Image src={groupWinner1} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={groupWinner2} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={groupWinner3} alt="carousel" />
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div>
                  <Image src={groupWinner4} alt="carousel" />
                </div>
              </SwiperSlide>
            </Swiper>
            <div className="w-4/12  -mt-1 z-50 text-center">
              <Lottie animationData={swipe} loop={true} />
            </div>
          </div>
        </div>

        <div
          id="videos"
          className="flex flex-col gap-5 items-center justify-center border-b-2 py-10 border-gray-300"
        >
          <div className="text-[30px] font-bold font-Montserrat">Videos</div>
          <div className="flex gap-2">
            <div className="rounded-xl overflow-hidden w-full">
              <Link
                target="_blank"
                href={"https://www.youtube.com/watch?v=olDWm2veCrM"}
              >
                <Image
                  className="hover:scale-110 transition-all duration-200 ease-out"
                  src={vid1}
                  alt="vid1"
                />
              </Link>
            </div>
            <div className="rounded-xl overflow-hidden w-full">
              <Link
                target="_blank"
                href={"https://www.youtube.com/watch?v=GsV1i0QHi-o"}
              >
                <Image
                  className="hover:scale-110 transition-all duration-200 ease-out"
                  src={vid2}
                  alt="vid2"
                />
              </Link>
            </div>
          </div>
        </div>

        {/* Layout */}
        <div
          id="layout"
          className="flex flex-col gap-5 items-center justify-center border-b-2 py-10 border-gray-300"
        >
          <div className="text-[30px] font-bold font-Montserrat">Layout</div>
          <div className="w-full flex justify-center items-center">
            <Image className="w-full " src={layout} alt="layout" />
          </div>

          <div className="w-full flex flex-col justify-center items-center gap-2 pb-10">
            <h3 className="text-[30px] font-bold">Concert Information</h3>
            <div className="">
              <h3>Date: Saturday, June 8 2024</h3>
            </div>
            <div>
              <h3>Venue: Tennis Indoor Senayan</h3>
            </div>
            <div>
              <h3>City: Jakarta</h3>
            </div>
            <div>
              <h3>Ticket Vendor: tiket.com</h3>
            </div>
          </div>
          <div className="flex flex-col items-center gap-3">
            <button
              onClick={() => {
                router.push(
                  "https://tiket.com/to-do/2024-baby-monster-first-fan-meet-in-jakarta-see-you-there"
                );
              }}
              className={`px-12  py-4 rounded-md text-white font-bold font-Montserrat
              ${
                isButtonDisabled == false
                  ? "bg-slate-700 hover:bg-slate-500  transition-all duration-200 ease-out hover:scale-105"
                  : "bg-slate-500"
              }
              `}
            >
              General On-Sale
            </button>

            <div className="text-[12px] text-center">
              <p>
                Starts on Thursday, 2 May 2024 at 10AM<br></br> (Jakarta Local
                Time)
              </p>
            </div>
          </div>
        </div>

        {/* Instruction */}
        <div
          id="sales"
          className="flex flex-col gap-5 w-full items-center border-b-2 py-10 border-gray-300"
        >
          <div className="text-[30px] font-bold font-Montserrat text-center">
            Ticket Sales Instruction
          </div>
          <div className="lg:w-6/12 w-full justify-center   flex flex-col lg:flex-row gap-5">
            <div className="lg:w-7/12 flex flex-col gap-5">
              <div className="flex flex-col">
                <div className="font-bold text-[20px]">
                  <h3 className="cursor-pointer transition-all duration-200 hover:pl-3">
                    Step 1
                  </h3>
                </div>
                <div>
                  <p className="text-[14px] lg:text-[15px]">
                    Visit www.babymonsterinjakarta.com
                    <br /> Select <strong>TICKETS</strong>.
                  </p>
                </div>
              </div>
              <div className="flex flex-col">
                <div className="font-bold text-[20px]">
                  <h3 className="cursor-pointer transition-all duration-200 hover:pl-3">
                    Step 2
                  </h3>
                </div>
                <div>
                  <p className="text-[14px] lg:text-[15px]">
                    Click <strong>General On-Sale</strong>. You will be <br />
                    directed to the waiting room.
                  </p>
                </div>
              </div>
              <div className="flex flex-col">
                <div className="font-bold text-[20px]">
                  <h3 className="cursor-pointer transition-all duration-200 hover:pl-3">
                    Step 3
                  </h3>
                </div>
                <div>
                  <p className="text-[14px] lg:text-[15px]">
                    Select your preferred category
                    <br />
                    *Important
                    <br />
                    <br />
                    <strong>1 (one) transaction</strong> is limited to{" "}
                    <strong>6 (six) tickets</strong>.<br />
                    <br />
                    <strong>1 (one) transaction</strong> is limited to{" "}
                    <strong>1 (one) ticket category</strong>
                    <br />
                    <br />
                    <strong>1 (one) email address</strong> and{" "}
                    <strong>1 (one) phone number</strong> is limited to{" "}
                    <strong>1 (one) transaction</strong>
                  </p>
                </div>
              </div>
              <div className="flex flex-col">
                <div className="font-bold text-[20px]">
                  <h3 className="cursor-pointer transition-all duration-200 hover:pl-3">
                    Step 4
                  </h3>
                </div>
                <div>
                  <p className="text-[14px] lg:text-[15px]">
                    Fill in your <strong>personal information</strong>{" "}
                    correctly. Make sure your data, such as ID Number, email
                    address and phone number are correct.
                    <br />
                    <br />
                    Click <strong>Continue to Payment</strong>.
                  </p>
                </div>
              </div>
            </div>
            <div className="lg:w-6/12 flex flex-col gap-5">
              <div className="flex flex-col">
                <div className="font-bold text-[20px]">
                  <h3 className="cursor-pointer transition-all duration-200 hover:pl-3">
                    Step 5
                  </h3>
                </div>
                <div>
                  <p className="text-[14px] lg:text-[15px]">
                    Select your preferred payment method.
                    <br />
                    <br />
                    The time limit to complete your payment is{" "}
                    <strong>30 minutes</strong>.
                    <br />
                    <br />
                    Payment methods are available using Virtual Account Bank
                    Transfer, Debit/Credit Card (VISA, Mastercard, American
                    Express, JCB), and Paylater (Blibli Tiket Paylater, Kredivo,
                    Akulaku).
                  </p>
                </div>
              </div>
              <div className="flex flex-col">
                <div className="font-bold text-[20px]">
                  <h3 className="cursor-pointer transition-all duration-200 hover:pl-3">
                    Step 6
                  </h3>
                </div>
                <div>
                  <p className="text-[14px] lg:text-[15px]">
                    Please make sure your data, such as ticket category,
                    customer data, and payment method are correct before
                    payment.
                  </p>
                </div>
              </div>
              <div className="flex flex-col">
                <div className="font-bold text-[20px]">
                  <h3 className="cursor-pointer transition-all duration-200 hover:pl-3">
                    Step 7
                  </h3>
                </div>
                <div>
                  <p className="text-[14px] lg:text-[15px]">
                    <strong>E-Voucher</strong> will be available at{" "}
                    <strong>“your order”</strong> page and sent to the buyer's{" "}
                    <strong>registered email address</strong> once your payment
                    is marked as successful.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Terms & Condition */}
        <div
          id="term"
          className="flex flex-col gap-5  border-b-2 py-10 border-gray-300"
        >
          <div className="text-[30px] font-bold text-center font-Montserrat">
            Terms & Conditions
          </div>

          {/* General */}
          <ol className={` ${showAll ? "show" : "hide"}`}>
            <div className="py-3">
              <h3 className="font-bold text-[19px]">General</h3>
            </div>
            <ol
              className={`list-decimal text-[16px] lg:px-10 px-8 flex flex-col gap-1`}
            >
              <li>
                <p>
                  Your Ticket/s are sold by the Promoters directly to the
                  consumer. Any tickets purchased by businesses or traders in
                  breach of the Terms & Conditions of ticket sale will be
                  canceled. By accepting these terms and conditions you confirm
                  that you are a consumer.
                </p>
                <p className="text-red-500 italic">
                  Tiket dijual secara langsung oleh Promotor ke konsumen. Tiket
                  apa pun yang dibeli oleh pelaku usaha atau pedagang yang
                  melanggar Syarat & Ketentuan penjualan tiket akan dibatalkan.
                  Dengan menerima syarat dan ketentuan ini, Anda mengkonfirmasi
                  bahwa Anda adalah konsumen.
                </p>
              </li>
              <li>
                <p>
                  Tickets cannot be used for commercial purposes, including but
                  not limited to prizes, competition, contests or sweepstakes.
                  Tickets sold or used in breach of this condition may be
                  nullified without a refund, where the Ticket Holder will be
                  refused admission into the fanmeet area with no exception. The
                  Organizer is not responsible for the negligence of the Ticket
                  Buyer which results in the ticket falling into the hands of
                  other parties which can be used as an entry requirement,
                  thereby eliminating the Ticket Buyer’s rights to enter the
                  venue area.
                </p>
                <p className="text-red-500 italic">
                  Tiket tidak dapat digunakan untuk keperluan komersial,
                  termasuk namun tidak terbatas kepada hadiah, kompetisi,
                  kontes, atau undian. Tiket yang dijual atau digunakan dengan
                  melanggar persyaratan ini dapat dibatalkan tanpa pengembalian
                  dana, dan Pemegang Tiket akan ditolak masuk ke dalam area
                  acara tanpa terkecuali. Penyelenggara tidak bertanggung jawab
                  atas kelalaian Pembeli Tiket yang mengakibatkan Tiket jatuh ke
                  tangan orang lain dan dipergunakan sebagai tanda masuk,
                  sehingga menghilangkan hak Pembeli Tiket untuk masuk ke area
                  acara.
                </p>
              </li>
              <li>
                <p>
                  Your Ticket/s will IMMEDIATELY BECOME INVALID if resold OR
                  OFFERED FOR SALE. Tickets sold via third parties and other
                  unauthorized outlets, including online auction sites (such as
                  Viagogo, StubHub, tiketX, thekaryll, etc.), are not valid for
                  admission. The resale of a Ticket renders it invalid and may
                  lead to refusal of entry.
                </p>
                <p className="text-red-500 italic">
                  Tiket Anda LANGSUNG MENJADI TIDAK VALID jika dijual kembali
                  ATAU DITAWARKAN UNTUK DIJUAL. Tiket yang dijual melalui pihak
                  ketiga dan outlet tidak resmi lainnya, termasuk situs lelang
                  online (seperti Viagogo, Stubhub, tiketX, thekaryll, dll),
                  tidak berlaku untuk tiket masuk. Penjualan kembali Tiket
                  menjadikannya tidak sah dan dapat menyebabkan penolakan masuk.
                </p>
              </li>
              <li>
                <p>
                  Every Ticket Holder who comes to the event area is responsible
                  for their own safety, health and personal security. The
                  Sponsors/Band/Organizer/Management are not responsible for,
                  including but not limited to any injury or damage that may
                  occur to Ticket Holders during or in the event.
                </p>

                <p className="text-red-500 italic">
                  Setiap Pemegang Tiket yang datang ke area acara bertanggung
                  jawab atas keselamatan, kesehatan, dan keamanan pribadinya
                  sendiri. Para Sponsor/Band/Penyelenggara/Manajemen tidak
                  bertanggung jawab untuk, termasuk namun tidak terbatas pada
                  cedera atau kerusakan yang mungkin terjadi kepada Pemegang
                  Tiket selama atau di dalam acara tersebut.
                </p>
              </li>
              <li>
                <p>
                  Your Ticket purchase constitutes a personal, revocable license
                  and, at all times, remains the property of the promoters.
                </p>
                <p className="text-red-500 italic">
                  Tiket Anda merupakan lisensi pribadi yang dapat dibatalkan
                  dan, sepanjang waktu, tetap menjadi milik promotor.
                </p>
              </li>
              <li>
                <p>
                  All ticket sales are final. Tickets that have been sold cannot
                  be exchanged and unable to be cashed out.
                </p>
                <p className="text-red-500 italic">
                  Semua penjualan Tiket adalah final. Tiket yang telah terjual
                  tidak dapat ditukar dan tidak dapat diuangkan kembali.
                </p>
              </li>
              <li>
                <p>
                  By purchasing the tickets to the BABYMONSTER First Fanmeet
                  “See You There” 2024 IN JAKARTA. you agree to the collection,
                  storage and limited use of your personal data for the purposes
                  of the BABYMONSTER First Fanmeet “See You There” 2024 IN
                  JAKARTA.
                </p>
                <p className="text-red-500 italic">
                  Dengan membeli Tiket acara BABYMONSTER First Fanmeet “See You
                  There” 2024 IN JAKARTA, Anda telah menyetujui pengumpulan,
                  penyimpanan dan penggunaan data pribadi Anda secara terbatas
                  untuk keperluan acara BABYMONSTER First Fanmeet “See You
                  There” 2024 IN JAKARTA
                </p>
              </li>
              <li>
                <p>
                  By purchasing tickets to the BABYMONSTER First Fanmeet “See
                  You There” 2024 IN JAKARTA, Ticket Buyers and Ticket Holders
                  agree to comply with all the Terms & Conditions of the event
                  that are determined and valid from time to time.
                </p>
                <p className="text-red-500 italic">
                  Dengan membeli Tiket untuk ke acara fanmeet BABYMONSTER First
                  Fanmeet “See You There” 2024 IN JAKARTA, maka Pembeli Tiket
                  dan Pemegang Tiket setuju untuk menaati setiap dan seluruh
                  Syarat & Ketentuan acara yang ditetapkan dan berlaku dari
                  waktu ke waktu.
                </p>
              </li>
              <li>
                <p>
                  The Ticket Holder grants the Organizer and Sponsors the right
                  to use, in perpetuity, all or any part of the recording of any
                  video and still footage made of the Ticket holder’s appearance
                  on any channels, including but not limited to magazine, social
                  media, and TV for broadcast in any and all media globally used
                  for advertising, publicity and promotions relating thereto
                  without any further approval of yours.
                </p>
                <p className="text-red-500 italic">
                  Pemegang Tiket memberikan hak kepada Penyelenggara acara dan
                  para Sponsor untuk melakukan rekaman video dan gambar baik
                  sebagian atau semua, pada kanal/platform apapun, termasuk
                  namun tidak terbatas pada majalah, media sosial, dan saluran
                  TV untuk disiarkan di media apapun di seluruh dunia, untuk
                  iklan, publisitas dan promosi yang berkaitan dengan konser
                  tanpa persetujuan lebih lanjut dari Pemegang Tiket.
                </p>
              </li>
              <li>
                <p>
                  The Organizer has the right to refuse admission to and/or
                  evict Ticket-Holders who do not abide by the Terms and
                  Conditions of the event.
                </p>
                <p className="text-red-500 italic">
                  Penyelenggara acara memiliki hak untuk menolak masuk dan/atau
                  mengeluarkan orang-orang yang tidak menaati syarat dan
                  ketentuan acara.
                </p>
              </li>
              <li>
                <p>
                  The Organizer has the right to process and prosecute in
                  accordance with the provisions of the prevailing laws and
                  regulations, both civil procedure and criminal procedure
                  against people who obtain the tickets through illegal ways,
                  including but not limited to forging or duplicating valid
                  tickets, or obtain the tickets in a way that is not in
                  accordance with what has been determined by the Organizer as
                  stated in these Terms and Conditions.
                </p>
                <p className="text-red-500 italic">
                  Penyelenggara berhak untuk memproses dan menuntut secara hukum
                  sesuai dengan ketentuan peraturan perundang-undangan yang
                  berlaku baik secara perdata maupun secara pidana terhadap
                  orang-orang yang memperoleh Tiket dengan cara yang tidak sah
                  termasuk namun tidak terbatas pada cara melakukan pemalsuan
                  atau menggandakan Tiket yang sah atau memperoleh Tiket dengan
                  cara yang tidak sesuai dengan yang telah ditentukan oleh
                  Penyelenggara sebagaimana dalam Syarat dan Ketentuan ini.
                </p>
              </li>
              <li>
                <p>
                  The organizer reserves all rights to change/add or modify all
                  provisions mentioned in the Terms and Conditions without
                  notice.
                </p>
                <p className="text-red-500 italic">
                  Penyelenggara berhak untuk mengubah/menambah atau memodifikasi
                  semua ketentuan yang disebutkan dalam Syarat dan Ketentuan
                  tanpa pemberitahuan terlebih dahulu.
                </p>
              </li>
              <li>
                <p>
                  Please check our Instagram account (@pkentertainment.id and
                  @temgmt) periodically for more information about BABYMONSTER
                  First Fanmeet “See You There” 2024 IN JAKARTA.
                </p>
                <p className="text-red-500 italic">
                  Mohon cek akun Instagram kami (@pkentertainment.id and
                  @temgmt) secara berkala untuk informasi lebih lanjut seputar
                  acara BABYMONSTER First Fanmeet “See You There” 2024 IN
                  JAKARTA.
                </p>
              </li>
            </ol>

            <div className="py-3">
              <h3 className="font-bold text-[19px]">Ticketing</h3>
            </div>

            <ol
              className={`list-decimal text-[16px] lg:px-10 px-8  flex flex-col gap-1`}
            >
              <li>
                <p>
                  Tickets can only be purchased through
                  babymonsterinjakarta.com. Tickets are only for admission to
                  the BABYMONSTER First Fanmeet “See You There” 2024 IN JAKARTA.
                </p>
                <p className="text-red-500 italic">
                  Tiket hanya dapat dibeli melalui babymonsterinjakarta.com.
                  Tiket dapat digunakan untuk masuk ke acara fanmeet BABYMONSTER
                  First Fanmeet “See You There” 2024 IN JAKARTA.
                </p>
              </li>
              <li>
                <p>
                  Ticket price excludes Government Tax 10%, Platform Fee 5% and
                  Convenience Fee.
                </p>
                <p className="text-red-500 italic">
                  Harga Tiket tidak termasuk Pajak 10%, Biaya Platform 5% dan
                  biaya lainnya.
                </p>
              </li>
              <li>
                <p>
                  Ticket sales are limited to a maximum of 6 (six) per
                  transaction within the same category. 1 (one) email address
                  and 1 (one) phone number is limited to 1 (one) transaction.
                </p>
                <p className="text-red-500 italic">
                  Penjualan Tiket dibatasi maksimal 6 (enam) per transaksi dalam
                  kategori yang sama. 1 (satu) alamat email dan 1 (satu) nomor
                  telepon dibatasi 1 (satu) transaksi.
                </p>
              </li>
              <li>
                <p>
                  Name in accordance with a valid ID Card is required to
                  purchase tickets to the BABYMONSTER First Fanmeet “See You
                  There” 2024 IN JAKARTA. Make sure to buy tickets using your
                  valid and correct matching data (ID Card/KK/KTP/SIM/Passport).
                  Your ticket(s) cannot be changed and/or modified once the
                  purchase has been made.
                </p>
                <p className="text-red-500 italic">
                  Nama sesuai dengan kartu identitas yang sah bersifat wajib
                  untuk membeli Tiket ke acara fanmeet BABYMONSTER First Fanmeet
                  “See You There” 2024 IN JAKARTA. Pastikan Anda melakukan
                  pembelian Tiket dengan menggunakan data Anda yang sah dan
                  benar (Kartu Identitas/KK/KTP/SIM/Paspor). Tiket Anda tidak
                  dapat diubah dan/atau dimodifikasi setelah pembelian sudah
                  dilakukan.
                </p>
              </li>
              <li>
                <p>
                  This fanmeet consists of 4 (four) main categories. <br />
                  a. Pink and Yellow are numbered tribune seating categories,
                  the Seat number(s) will be provided closer to the event date.{" "}
                  <br />
                  b. Blue (Regular) and Blue (VIP) are free standing categories.
                  Queue number(s) will be provided upon ticket purchase for all
                  free standing categories, and will be implemented on the show
                  day. <br />
                </p>
                <p className="text-red-500 italic">
                  Fanmeet ini memiliki 4 (tiga) kategori utama. <br />
                  a. Pink dan Kuning adalah area duduk bernomor dan bertingkat,
                  Nomor kursi akan didapatkan menjelang hari acara. <br />
                  b. Kategori Biru (Reguler) dan Blue (VIP) adalah kategori
                  berdiri. Nomor antrian akan tersedia pada E-Voucher yang akan
                  didapatkan disaat pembelian, dan akan di implementasi pada
                  hari acara. <br />
                </p>
              </li>
              <li>
                <p>
                  The benefits of Blue (VIP) package are:
                  <br />
                  a. One Blue (Festival) Ticket
                  <br />
                  ⁠b. ⁠Hi Bye (All)
                  <br />
                  ⁠c. ⁠Group Photo (Raffle - 150 pax)
                  <br />
                  ⁠d. ⁠Signed Poster (Raffle - 350 pax)
                  <br />
                  ⁠⁠e. Exclusive Photo Card (All)
                  <br />
                  ⁠⁠f. Lanyard and Laminate (All)
                  <br />
                </p>
                <p className="text-red-500 italic">
                  Keuntungan yang didapatkan dari kategori Blue VIP adalah:
                  <br />
                  a. 1 Tiket (Festival) Blue
                  <br />
                  b. Hi Bye (Semua)
                  <br />
                  c. Group Photo (Raffle - 150 pax)
                  <br />
                  d. ⁠⁠Signed Poster (Raffle - 350 pax)
                  <br />
                  e. ⁠⁠Exclusive Photo Card (Semua)
                  <br />
                  f. Lanyard and Laminate (Semua)
                  <br />
                </p>
              </li>
              <li>
                <p>
                  The Ticket Holder must occupy the seat or position that has
                  been assigned by the Organizer according to the category of
                  the ticket purchased.
                </p>
                <p className="text-red-500 italic">
                  Pemegang Tiket wajib menempati tempat duduk atau posisi yang
                  telah ditentukan oleh pihak Penyelenggara sesuai dengan
                  kategori Tiket yang dibeli.
                </p>
              </li>
              <li>
                <p>
                  In the event of a canceled fanmeet, tickets will be refunded
                  in accordance with the provisions of the Organizer. Refunds do
                  not include Platform Fee, Convenience Fee, and any other
                  personal costs of the Ticket Buyer (e.g. travel expenses,
                  accommodation expenses, etc).
                </p>
                <p className="text-red-500 italic">
                  Jika terjadi pembatalan fanmeet, maka Tiket akan dikembalikan
                  sesuai dengan ketentuan Penyelenggara. Pengembalian harga
                  Tiket tidak termasuk Biaya Platform, Biaya Lainnya dan biaya
                  pribadi Pembeli Tiket (contoh biaya perjalanan, biaya
                  akomodasi, dll).
                </p>
              </li>
            </ol>

            <div className="py-3">
              <h3 className="font-bold text-[19px]">Wristband Redemption</h3>
            </div>
            <ol
              className={`list-decimal text-[16px] lg:px-10 px-8  flex flex-col gap-1`}
            >
              <li>
                <p>
                  The time and place for the wristband redemption will be
                  announced and informed through the promoter’s social
                  networking services (@pkentertainment.id and @temgmt), as well
                  as the official website www.babymonsterinjakarta.com. It is
                  recommended to exchange the e-voucher beforehand to avoid the
                  queue on the day of the concert
                </p>
                <p className="text-red-500 italic">
                  Waktu dan tempat penukaran gelang akan diumumkan dan
                  diinformasikan melalui media sosial promotor
                  (@pkentertainment.id dan @temgmt), serta situs web resmi
                  www.babymonsterinjakarta.com. Disarankan untuk menukarkan
                  e-voucher sebelumnya untuk menghindari antrian pada hari
                  konser.
                </p>
              </li>
              <li>
                <p>
                  In order for Tickets to be valid for the wristband redemption,
                  , the Ticket Buyer (Lead Booker) will be asked to provide all
                  of the following items alongside their ticket(s):
                  <br />
                  a. Photo ID (ID Card/KK/KTP/SIM/Passport)
                  <br />
                  b. Tiket.com E-Voucher
                  <br />
                </p>
                <p className="text-red-500 italic">
                  Agar Tiket Anda valid, di hari acara, Pembeli Tiket (Pemesan
                  Utama) akan diminta untuk menyediakan barang dibawah ini
                  bersama tiket Anda:
                  <br />
                  a. Kartu identitas asli (ID Card/KK/KTP/SIM/Paspor)
                  <br />
                  b. E-Voucher Tiket.com
                  <br />
                </p>
                <p>
                  If the wristband redemption is represented, please provide:
                  <br />
                  a. Printed e-voucher <br />
                  b. Copy of the Photo ID (ID Card/KK/KTP/SIM/Passport) of the
                  original ticket holder
                  <br />
                  c. Letter of Attorney signed on a Rp.10.000 Duty Stamp by the
                  represented party
                  <br />
                </p>
                <p className="text-red-500 italic">
                  Apabila penukaran wristband diwakili, mohon untuk melampirkan:
                  <br />
                  a. E-voucher yang telah dicetak
                  <br />
                  b. Fotokopi identitas diri (KTP/KK/SIM/Paspor) pemesan tiket
                  asli
                  <br />
                  c. Surat Kuasa yang ditandatangani di atas materai Rp.10.000
                  oleh pihak yang diwakili
                  <br />
                </p>
              </li>
              <li>
                <p>
                  The organizer will not provide any wristband replacements for
                  lost and damaged for any reason.
                </p>
                <p className="text-red-500 italic">
                  Penyelenggara tidak akan memberikan penggantian wristband
                  tersebut hilang atau rusak karena alasan apa pun.
                </p>
              </li>
            </ol>

            <div className="py-3">
              <h3 className="font-bold text-[19px]">Health and Safety</h3>
            </div>
            <ol
              className={`list-decimal text-[16px] lg:px-10 px-8  flex flex-col gap-1`}
            >
              <li>
                <p>
                  For audiences who feel unwell, it is highly recommended to
                  wear a mask throughout the event.
                </p>
                <p className="text-red-500 italic">
                  Bagi penonton yang merasa kurang sehat, sangat disarankan
                  untuk menggunakan masker selama acara berlangsung.
                </p>
              </li>
              <li>
                <p>
                  The organizer reserves all rights to add any additional Terms
                  / Conditions related to health protocols in accordance with
                  the prevailing law and/or regulations at the time of the
                  event.
                </p>
                <p className="text-red-500 italic">
                  Penyelenggara berhak untuk menambahkan Syarat/Ketentuan
                  tambahan terkait protokol kesehatan sesuai dengan hukum
                  dan/atau peraturan yang berlaku pada saat acara berlangsung.
                </p>
              </li>
            </ol>

            <div className="py-3">
              <h3 className="font-bold text-[19px]">
                Special Needs Assistance
              </h3>
            </div>
            <ol
              className={`list-decimal text-[16px] lg:px-10 px-8  flex flex-col gap-1`}
            >
              <li>
                <p>
                  Only Blue (Regular) tickets can be accessed by attendees with
                  special needs using wheelchairs. The organizer will create a
                  platform in the festival area specifically designed for
                  attendees using wheelchairs.
                </p>
                <p className="text-red-500 italic">
                  Hanya kategori tiket Blue (Reguler) yang dapat diakses oleh
                  pengunjung berkebutuhan khusus yang menggunakan kursi roda.
                  Penyelenggara akan menyediakan platform di area festival yang
                  dirancang khusus untuk pengunjung yang menggunakan kursi roda.
                </p>
              </li>
              <li>
                <p>
                  There is a limited quota for wheelchair accessible seating.
                  For more information regarding wheelchair accessible seating
                  and booking, please contact Tiket.com customer service at +62
                  804 1500 878 or cs@tiket.com. to process your booking. To get
                  your booking authorized, you must present your medical
                  certificate as the proof of disability. The Organizer only
                  accepts a legitimate medical certificate from a legitimate
                  institution and physician.
                </p>
                <p className="text-red-500 italic">
                  Terdapat kuota dalam jumlah terbatas untuk tempat duduk yang
                  dapat diakses dengan kursi roda. Untuk informasi lebih lanjut
                  dan pemesanan, silakan hubungi layanan pelanggan Tiket.com +62
                  804 1500 878 atau cs@tiket.com. di untuk memproses pemesanan
                  Anda. Untuk mendapatkan otorisasi pemesanan tempat duduk ini,
                  Anda diwajibkan untuk melampirkan Surat Keterangan Dokter yang
                  sah. Penyelenggara Acara hanya menerima Surat Keterangan
                  Dokter yang berasal dari institusi yang sah, baik klinik
                  maupun rumah sakit.
                </p>
              </li>
            </ol>

            <div className="py-3">
              <h3 className="font-bold text-[19px]">Age Restriction</h3>
            </div>
            <ol
              className={`list-decimal text-[16px] lg:px-10 px-8  flex flex-col gap-1`}
            >
              <li>
                <p>
                  Children under the age of 14 (fourteen) must be accompanied by
                  a parent or legal guardian aged 18 (eighteen) or over.
                  Children under the age of 6 (six) are not allowed to attend
                  the event.
                </p>
                <p className="text-red-500 italic">
                  Anak-anak di bawah usia 14 (empat belas) tahun harus
                  didampingi oleh orang tua atau wali sah yang berusia 18
                  (delapan belas) tahun atau lebih. Anak-anak di bawah usia 6
                  (enam) tahun tidak diperbolehkan masuk ke area acara.
                </p>
              </li>
            </ol>

            <div className="py-3">
              <h3 className="font-bold text-[19px]">No Re-Entry Policy</h3>
            </div>
            <ol
              className={`list-decimal text-[16px] lg:px-10 px-8  flex flex-col gap-1`}
            >
              <li>
                <p>
                  Re-entry to the venue is not permitted. This applies to all
                  ticket holders and ticket categories. Once you enter, you
                  won't be able to leave and re-enter. The organizer reserves
                  the right to deny venue access, even to holders of valid
                  tickets/wristbands with barcodes, if the ticket/wristband has
                  already been scanned.
                </p>
                <p className="text-red-500 italic">
                  Tidak diperbolehkan untuk keluar masuk area konser. Hal ini
                  berlaku untuk semua pemegang tiket dan kategori tiket. Setelah
                  Anda masuk, Anda tidak dapat keluar dan masuk kembali.
                  Penyelenggara berhak menolak akses masuk ke dalam venue,
                  bahkan kepada pemegang tiket/gelang yang masih berlaku dengan
                  barcode, jika tiket/gelang tersebut telah dipindai.
                </p>
              </li>
            </ol>

            <div className="py-3">
              <h3 className="font-bold text-[19px]">
                Security Check and Prohibited Items
              </h3>
            </div>
            <ol
              className={`list-decimal text-[16px] lg:px-10 px-8  flex flex-col gap-1`}
            >
              <li>
                <p>
                  We take your safety seriously. To ensure a smooth and secure
                  entry process for everyone, we will be conducting thorough
                  security checks, which may include:
                </p>
                <p className="text-red-500 italic">
                  Kami sangat memprioritaskan keamanan Anda. Untuk memastikan
                  proses masuk yang lancar dan aman bagi semuanya, kami akan
                  melakukan pemeriksaan keamanan secara menyeluruh, termasuk:
                </p>
                <ul>
                  <li>
                    <p>Visual inspections of bags and belongings.</p>
                    <p className="text-red-500 italic">
                      Inspeksi visual terhadap tas dan barang bawaan.
                    </p>
                  </li>
                  <li>
                    <p>Airport-style pat-down searches.</p>
                    <p className="text-red-500 italic">
                      Pemeriksaan dengan metode bandara
                    </p>
                  </li>
                  <li>
                    <p>Electronic drug detection and K-9 screenings.</p>
                    <p className="text-red-500 italic">
                      Detektor obat – obatan terlarang dan anjing pelacak (K-9).
                    </p>
                  </li>
                </ul>
              </li>
              <li>
                <p>
                  You have the right to refuse a search, but the organizer may
                  deny you entry if you do.
                </p>
                <p className="text-red-500 italic">
                  Anda memiliki hak untuk menolak pemeriksaan, tetapi
                  penyelenggara dapat menolak Anda untuk masuk apabila Anda
                  melakukannya.
                </p>
              </li>
              <li>
                <p>
                  All ticket holders must comply with the following guidelines
                  for permitted bags:
                </p>
                <p className="text-red-500 italic">
                  Seluruh pemegang tiket wajib mengikuti panduan untuk tas yang
                  diizinkan sebagai berikut:
                </p>
                <ul>
                  <li>
                    <p>
                      Only transparent PVC bags meeting specified criteria are
                      permitted to enter the concert venue. Any bags made of
                      materials other than transparent PVC will not be allowed,
                      the security team retains the authority to refuse entry to
                      the venue.
                    </p>
                    <p className="text-red-500 italic">
                      Hanya tas PVC transparan yang memenuhi kriteria yang
                      diperbolehkan masuk ke area konser. Tas apapun yang
                      terbuat dari bahan selain PVC transparan tidak akan
                      diizinkan, tim keamanan memiliki wewenang untuk menolak
                      masuk ke tempat acara.
                    </p>
                  </li>
                  <li>
                    <p>
                      PVC bags must not exceed the designated size limit, which
                      is no larger than A4 size or 210mm x 297mm.
                    </p>
                    <p className="text-red-500 italic">
                      Tas PVC tidak diizinkan untuk melebihi batas ukuran yang
                      ditentukan, yaitu tidak lebih besar dari ukuran A4 atau
                      210 mm x 297 mm.
                    </p>
                  </li>
                  <li>
                    <p>
                      Event attendees are advised not to bring more than one (1)
                      bag, as the organizer does not offer a deposit box
                      counter/booth.
                    </p>
                    <p className="text-red-500 italic">
                      Penonton tidak diperkenankan membawa lebih dari satu (1)
                      tas. Penyelenggara tidak menyediakan konter/tempat
                      penitipan barang.
                    </p>
                  </li>
                </ul>
              </li>
              <li>
                <p>
                  The following items are not permitted in the event and may be
                  ejected with or without the owner from the venue area:
                </p>
                <p className="text-red-500 italic">
                  Barang-barang berikut tidak diizinkan dalam acara dan akan
                  dikeluarkan dengan atau tanpa pemilik dari area acara
                  tersebut:
                </p>
                <ul>
                  <li>
                    <p>
                      Non-authorized professional recording equipment
                      (professional cameras, video recording devices, and audio
                      recording devices, including ‘GoPro’ style devices and
                      tablets), for unauthorized people/journalist without
                      accredited badge;
                    </p>
                    <p className="text-red-500 italic">
                      Alat perekam profesional yang tidak memiliki izin (kamera
                      profesional, alat perekam video dan alat perekam suara,
                      termasuk perangkat sejenis GoPro dan tablet) untuk pihak
                      yang tidak resmi / wartawan yang tidak sah tanpa ID khusus
                      dari Penyelenggara;
                    </p>
                  </li>
                  <li>
                    <p>
                      Liquids, alcohol, cigarettes and banned substances
                      including outside food and drink;
                    </p>
                    <p className="text-red-500 italic">
                      Cairan, alkohol, segala jenis rokok, dan barang-barang
                      terlarang lainnya termasuk makanan dan minuman;
                    </p>
                  </li>
                  <li>
                    <p>
                      Pyrotechnic items or explosives including fireworks,
                      candles, lighters;
                    </p>
                    <p className="text-red-500 italic">
                      Benda piroteknik atau benda yang bersifat meledak termasuk
                      kembang api, lilin atau korek api;
                    </p>
                  </li>
                  <li>
                    <p>
                      Flammable material including alcohol, gasoline, kerosene,
                      pressurized cologne;
                    </p>
                    <p className="text-red-500 italic">
                      Bahan mudah terbakar termasuk alkohol, bensin, minyak
                      tanah, cologne;
                    </p>
                  </li>
                  <li>
                    <p>
                      Dangerous or potentially hazardous objects, including but
                      not limited to sports items, weapons, knives, guns, laser
                      devices, iron bar, wooden bar, sharp object or material,
                      helmets, metal/plastic knuckles, self-defense spray (tear,
                      pepper-spray), stun guns (taser) or any electric shock
                      devices;
                    </p>
                    <p className="text-red-500 italic">
                      Benda berbahaya atau berpotensi berbahaya termasuk namun
                      tidak terbatas pada alat olahraga, senjata, pisau, senjata
                      api, perangkat laser, bar besi, bilah kayu, benda atau
                      bahan tajam, helm, keling besi/plastic, semprotan pertahan
                      diri (gas air mata, semprotan merica), senjata kejut
                      listrik atau perangkat sengatan listrik lainnya;
                    </p>
                  </li>
                  <li>
                    <p>
                      Any suspicious casual objects which can be used as a
                      weapon or projectile including tools, chemicals,
                      suspicious powder, umbrellas, selfie sticks, and other
                      poles or tripods.
                    </p>
                    <p className="text-red-500 italic">
                      Benda-benda kasual yang mencurigakan yang bisa dijadikan
                      senjata atau proyektil termasuk peralatan, bahan kimia,
                      bubuk mencurigakan, payung, tongkat swafoto dan
                      jenis-jenis tiang atau tripod lainnya.
                    </p>
                  </li>
                </ul>
              </li>
              <li>
                <p>
                  The promoter reserves the right to remove disruptive attendees
                  without a refund.
                </p>
                <p className="text-red-500 italic">
                  Penyelenggara berhak mengeluarkan pengunjung yang mengganggu
                  tanpa pengembalian dana.
                </p>
              </li>
              <li>
                <p>
                  The promoter is not responsible for lost or damaged
                  belongings.
                </p>
                <p className="text-red-500 italic">
                  Penyelenggara tidak bertanggung jawab atas barang yang hilang
                  atau rusak.
                </p>
              </li>
              <li>
                <p>
                  All attendees, including the media, are subject to these
                  security measures.
                </p>
                <p className="text-red-500 italic">
                  Semua pengunjung, termasuk media, wajib mengikuti tahapan
                  keamanan ini.
                </p>
              </li>
            </ol>
          </ol>

          <button className="text-blue-500" onClick={toggleShowAll}>
            {showAll ? "Show Less" : "Show More"}
          </button>
        </div>

        {/* Promoters */}
        <div
          id="promoters"
          className="flex flex-col gap-5 border-b-2 py-10 border-gray-300"
        >
          <div className="text-[30px] font-bold font-Montserrat text-center">
            Promoters
          </div>

          <div className="flex flex-col gap-1 justify-start items-start w-full">
            <div className="lg:w-2/12 w-full">
              <Image
                className="w-full"
                src={temLogo}
                alt="Third Eye Movement"
              />
            </div>
            <div>
              <p>
                TEM Presents is a powerhouse, live entertainment company
                established in 2007 by Samantha Tzovolos. TEM has promoted
                international artists in Indonesia such as John Mayer, Guns N’
                Roses, STING, Ariana Grande, Michael Buble, Backstreet Boys, The
                Chainsmokers, Il Divo, Mika, Keshi, LANY and more. For more
                information, please visit{" "}
                <Link
                  target="_blank"
                  className="font-bold"
                  href={"https://www.temgmt.com"}
                >
                  www.temgmt.com
                </Link>{" "}
                & Instagram{" "}
                <Link
                  target="_blank"
                  className="font-bold"
                  href={"https://instagram.com/temgmt"}
                >
                  @temgmt
                </Link>
                .
              </p>
            </div>
          </div>

          <div className="flex flex-col gap-1 justify-start items-start w-full">
            <div className="lg:w-4/12 w-full">
              <Image className="w-full" src={pkLogo} alt="PK Entertainment" />
            </div>
            <div className="flex-col flex gap-5">
              <p>
                PK Entertainment, Indonesia’s leading concert promoter and
                event, brand activation and creative agency was established in
                2015. We are a combination of visionaries, strategists, and
                creators who come together to curate and present top-notch
                quality events. Our unconventional thinking process engages with
                clients and their audience; ensuring that we meet all their
                requirements at the highest standards while maintaining their
                brand identity. We are an extension of your dream team and we
                aim to create a memorable experience for everyone.
              </p>

              <p>
                As an events, brand and creative agency; we have the experience
                of working together with multinational companies such as Google,
                Meta, Instagram, YouTube, Netflix, Spotify, GoTo and many more
                delivering over 300 plus events in the past 8 years.
              </p>

              <p>
                Celebrating our 8th Anniversary this year, we have an impressive
                portfolio of handling the biggest concerts in Jakarta including
                Celine Dion, Ed Sheeran, Backstreet Boys, Shawn Mendes, LANY,
                Keshi, Westlife, Calum Scott, RADWIMPS, Fujii Kaze, ONE OK ROCK,
                Charlie Puth, Coldplay, YOASOBI, ADO, Ed Sheeran, Tom Jones, and
                many more.
              </p>

              <p>
                It has been an amazing 2023 with more than 100,000 audiences,
                and will be a bigger 2024 with a long list of concerts, such as,
                RADWIMPS, Eve, BABYMONSTER, LANY, Anime Festival Asia 2024, and
                many more to be announced.
              </p>

              <p>
                For more information about us please visit{" "}
                <Link
                  target="_blank"
                  className="font-bold"
                  href={"https://www.pk-ent.com"}
                >
                  www.pk-ent.com
                </Link>{" "}
                or visit our Instagram{" "}
                <Link
                  target="_blank"
                  className="font-bold"
                  href={"https://instagram.com/pkentertainment.id"}
                >
                  @pkentertainment.id
                </Link>
                .
              </p>
            </div>
          </div>
        </div>

        <div
          id="contact"
          className="flex flex-col gap-5  py-10 border-gray-300"
        >
          <div className="text-[30px] font-bold font-Montserrat text-center pb-5">
            Customer Service Info
          </div>

          <div className="flex flex-col items-start justify-start gap-3">
            <h3 className="text-[17px]">
              For more information about ticket purchase and wheelchair
              accessible seating, please contact:
            </h3>

            <div className="flex flex-col gap-3">
              <h3 className="font-bold">Tiket.com</h3>
              <div>
                <h3>
                  Whatsapp:{" "}
                  <Link
                    className="underline"
                    href={"https://wa.me/6285811500888"}
                  >
                    +62858 1150 0888
                  </Link>
                </h3>
                <h3>
                  Phone:{" "}
                  <Link className="underline" href={"tel:628041500878"}>
                    +628 0415 00878
                  </Link>
                </h3>
                <h3>
                  Email:{" "}
                  <Link className="underline" href={"mailto:cs@tiket.com"}>
                    cs@tiket.com
                  </Link>
                </h3>
              </div>
            </div>
            <div className="flex flex-col gap-3">
              <h3 className="font-bold">PK Entertainment</h3>
              <div>
                <h3>
                  Email:{" "}
                  <Link
                    className="underline"
                    href={"mailto:contact@pk-ent.com"}
                  >
                    contact@pk-ent.com
                  </Link>
                </h3>
              </div>
            </div>
          </div>
        </div>

        <div id="copy" className="flex flex-col gap-3  py-10 border-gray-300">
          <div className="text-[18px] font-bold font-Montserrat text-center ">
            Copyright
          </div>
          <div className="text-[13px] font-bold font-Montserrat text-center ">
            © 2024 PK ENTERTAINMENT GROUP INDONESIA. ALL RIGHTS RESERVED.
          </div>
        </div>

        <button
          className={`${
            isVisible ? "block" : "hidden"
          } fixed bottom-10 right-10 w-12 h-12 rounded-full bg-blue-500 text-white flex justify-center items-center z-50`}
          onClick={scrollToTop}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M5 15l7-7 7 7"
            />
          </svg>
        </button>
      </div>
    </>
  );
}
