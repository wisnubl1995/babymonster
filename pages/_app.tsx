import "@/styles/globals.css";
import localFont from "@next/font/local";
import type { AppProps } from "next/app";

const poppins = localFont({
  src: [
    {
      path: "../public/fonts/basiercircle-regular-webfont.ttf",
      weight: "400",
    },
  ],
  variable: "--font-poppins",
});

export default function App({ Component, pageProps }: AppProps) {
  return (
    <div className={`${poppins.variable} font-sans`}>
      <Component {...pageProps} />
    </div>
  );
}
